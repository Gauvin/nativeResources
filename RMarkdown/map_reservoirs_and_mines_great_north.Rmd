---
title: "Untitled"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```





# Libs
```{r}

library(here)
library(sf)
library(areal)
 
library(patchwork)
library(tmap)
library(snapbox)

library(cancensus)
library(Census2SfSp)

library(magrittr)
library(tidyverse) 
library(glue)
library(reshape2)

source(here('R','postgis.R'))
source(here('R','st_scale_polygons.R'))
```

#Params
```{r}

cancensus_api_key <- keyringr::decrypt_gk_pw('cancensus_api_key key_value')
options(cancensus.api_key = cancensus_api_key)
options(cancensus.cache_path = "/home/charles/Projects/Census2SfSp/Cache")

mapBoxToken <- keyringr::decrypt_gk_pw('token mapToken')
Sys.setenv(MAPBOX_ACCESS_TOKEN=mapBoxToken)
  
db_port <- 5432
  
proj32198 <- "+proj=lcc +lat_1=60 +lat_2=46 +lat_0=44 +lon_0=-68.5 +x_0=0 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs"



```



 

# IO

## Dams

```{r}

con <- connectToDB(db='water',
                   port=db_port)


```

```{r}

#https://www.cehq.gouv.qc.ca/barrages/default.asp

queryStr <-  "SELECT * FROM public.\"reservoirs\";"

shp_dams <- sf::st_read(con,  query  = queryStr) %>% 
  sf::st_set_crs(4326)

```


## Rivers
```{r}

# Presently read in from postgis DB, but open data accessible through https://www.donneesquebec.ca/recherche/dataset/vmtl-hydrographie
# Not essential
queryStr <-  "SELECT * FROM public.\"hydro_l\";"

shp_rivers <- sf::st_read(con,  query  = queryStr) %>% 
  sf::st_set_crs(4326)

```

## Lakes
```{r}

# Presently read in from postgis DB, but open data accessible through https://www.donneesquebec.ca/recherche/dataset/vmtl-hydrographie
# Not essential
queryStr <-  "SELECT * FROM public.\"hydro_s\";"

shp_lakes <- sf::st_read(con,  query  = queryStr) %>% 
  sf::st_set_crs(4326)

```

## Mines

```{r}

# Same, read in from postgis but can be downloaded at https://www.donneesquebec.ca/recherche/dataset/activites-minieres
con_mines <- connectToDB(db='mines',
                   port=db_port)


```

```{r}

queryStr <-  "SELECT * FROM public.\"mines_et_projets\";"

shp_mines <- sf::st_read(con_mines,  query  = queryStr) %>% 
  sf::st_set_crs(4326)


```

 

## Wood processing plants
```{r}

dir_forest_industry <- here('Data','forest_industries')
dir.create( dir_forest_industry)
url <- 'ftp://transfert.mffp.gouv.qc.ca/Public/Diffusion/DonneeGratuite/Foret/TRANSFORMATION_BOIS/Usines_transformation_primaire/usinestransfo1.csv'

 
df_wood_industry <- SfSpHelpers::st_read_remote(url=url,
                            dirToDownload =dir_forest_industry)


```
 

## Provinces

```{r}

con_census <- connectToDB(db='census',
                   port=db_port)


```

```{r}

queryStr <-  "SELECT * FROM public.\"pr_c_Canada\";"

shp_prov <- sf::st_read(con_census,  query  = queryStr) %>% 
  sf::st_set_crs(4326)


shp_qc <- shp_prov %>% filter(name =='Quebec')
```

 
## Census with native population (proxied by language density)

```{r}
#Get DA level values along with the geography
shp_language <- get_census(dataset = 'CA16',
           level = 'DA',
           vectors = c("v_CA16_566","v_CA16_521"), 
           regions = list('PR'=24),
           labels = 'long',
           geo_format = 'sf'
           )




```

---
---


# Data manips


## Wood processing plants
```{r}

#Watch out the lng lat are encoded as factors ... 
vars_numeric <- c( 'longitude', 'latitude' , "volresper" ,"volfeuper", "qtedftde"  )
df_wood_industry %<>% mutate_at(vars_numeric , ~as.numeric(as.character(.x)) )


# see ftp://transfert.mffp.gouv.qc.ca/Public/Diffusion/DonneeGratuite/Foret/TRANSFORMATION_BOIS/Usines_transformation_primaire/description-des-champs.pdf
# and https://mffp.gouv.qc.ca/wp-content/uploads/portrait-statistique-2017.pdf for the 350 k cutoff
df_wood_industry_sawmills <- df_wood_industry %>% 
  filter(catcomplet == '2 — Industries du bois de sciage'&  produits =='Bois de sciage') %>% 
  filter(qtedftde >= 350000 )



assertthat::assert_that( df_wood_industry_sawmills$longitude %>% max < -65 & df_wood_industry_sawmills$longitude %>% min > -80)

shp_wood_industry_sawmills <- df_wood_industry_sawmills %>% st_as_sf(coords=c( 'longitude', 'latitude' ), 
                                                                     crs=4326) 

shp_wood_industry_sawmills %>% dim

```


---
---

## Population ecumene to compare against areas with native presence
```{r}

shp_language %<>% drop_na(any_of('Population'))

#https://github.com/mountainMath/dotdensity
shp_dots_all_pop <- dotdensity::compute_dots(shp_language, categories =  c( 'Population'), scale = 1)
shp_poly_pop_ecumen_heat <- SfSpHelpers::get_polygon_heatmap(shp_dots_all_pop,bw = 0.01, gsize = 1000)
shp_poly_pop_ecumen <- shp_poly_pop_ecumen_heat %>% st_union

 
```



## Filter large DAs for native presence 
```{r}

shp_language %<>% drop_na(any_of('v_CA16_566: Aboriginal languages'))

 
shp_dots_native <- dotdensity::compute_dots(shp_language, categories =  c( 'v_CA16_566: Aboriginal languages'), scale = 1) #more dots
shp_poly_pop_ecumen_heat_native <- SfSpHelpers::get_polygon_heatmap(shp_dots_native,bw = 0.01, gsize = 1000)
shp_poly_pop_ecumen_native <- shp_poly_pop_ecumen_heat_native %>% st_union


shp_language_native <- shp_poly_pop_ecumen_native

```
 
---
---

## Get reservoirs with the largest hydroelectrical capacity
```{r}

shp_dams_large_capacity_hydro <- shp_dams %>% 
  filter(grepl('Forte contenance' , `Catégorie Administrative` )) %>% 
  filter(Utilisation == "Hydroélectricité") %>% 
  filter(`Capacité de retenue                  (m3)` >= 5000*10**6) #5000 hm**3 (cubic hectometers -- this is huge and probably removes much of reservoirs in Outaouais an similar regions - Basktaton has close to 3000 hm**3 iirc)

shp_dams_large_capacity_hydro %>% dim
 
```







## Rivers associated with large reservoirs    

```{r}

shp_rivers_near_dam <- st_filter( shp_rivers %>% st_transform(crs=proj32198), 
                                  shp_dams_large_capacity_hydro %>% st_transform(crs=proj32198) %>% st_buffer(500)) %>% 
  st_transform(crs=4326)

shp_rivers_near_dam %>% dim

names_rivers <- shp_rivers_near_dam$hyl_nm_top
names_rivers <- names_rivers[!is.na(names_rivers)]

shp_rivers_near_dam_all_river <- shp_rivers %>% filter( hyl_nm_top %in% names_rivers)



```

---
---


## Tweak size of polygons and remove parts of polygon in water
```{r}

shp_lakes_qc <- st_filter(shp_lakes, shp_qc) %>%
  st_filter(shp_language_native_larger) %>% 
  st_union


#The scaling factor is not super rigorous, but is meant to readjust for the fact that the dotdensity for native vs overall population and normalized on different scales -- this is meant to avoid having to look for native pop ecumenes n the map  
shp_language_native_larger <-  st_union(shp_language_native) %>% st_sf %>%  scale_geom_df(scale_factor = 10)
 
shp_language_native_larger_removed <- st_difference( shp_language_native_larger, 
                                                     shp_lakes_qc )  

```

## Tweak the size of the population ecumen
```{r}

shp_poly_pop_ecumen_larger <- shp_poly_pop_ecumen %>%
  st_sf %>%  
  scale_geom_df(scale_factor = 5) %>% 
  st_intersection(shp_qc) #need to make it a bit larger since the pop occupies basically nothing compared to the entire territory %>% 


```

---
---


#Maps 

 

 
## Final map 
```{r}

#open ggplot legend glyphs
#rect: 0
#circle 1
#triangle:2

df1 <- shp_language_native_larger_removed %>% select( geometry)
df1$name <- 'Land with significant first\nnations presence'
df1$col <- 'red'
df1$shape <- 0

#Also adding rivers overburddens the map
# df2 <- shp_rivers_near_dam_all_river %>% select(geom) %>% rename(geometry=geom)
# df2$name <- 'Large hydroelectric\nreservoir and river'
# df2$col <- 'dodgerblue'
# df2$shape <- 0

df3 <- shp_dams_large_capacity_hydro %>% select(geometry)
df3$name <- 'Large hydroelectric\nreservoir'
df3$col <- 'dodgerblue'
df3$shape <- 0

df4 <-shp_mines %>% select(geometry)
df4$name <- 'Mines'
df4$col <- 'brown'
df4$shape <- 2

df5 <- shp_poly_pop_ecumen_larger  %>%   select( geometry)
df5$name <- 'Large population\ncenter'
df5$col <- 'purple'
df5$shape <- 0

df6 <- shp_wood_industry_sawmills %>% select(geometry)
df6$name <- 'Large sawmills'
df6$col <- 'darkgreen'
df6$shape <- 1
 

df_points<- do.call(rbind, list( df3,df6, df4))
df_poly <- do.call(rbind, list( df1,df5)) 
  
shp_buff <- shp_qc %>% st_buffer(0.1)

 
 
#mapbox_outdoors is nice for cartoraphic style maps 
p <- ggplot() + 
  layer_mapbox( map_style = mapbox_outdoors(), st_bbox(shp_buff) ) +    
  #
  #
  geom_sf(data=df_poly, aes(fill=name) , col='black', alpha=0.6, lwd=0.1  ) + 
  geom_sf(data=df_points, aes(col=name, shape=name) , key_glyph='point') + 
  #
  #
  scale_fill_manual(name='', values = unique(df_poly$col)  ) +
  scale_shape_manual(name = "",
                     values =  unique(df_points$shape)  ) + 
  scale_colour_manual(name='', 
                      values = unique(df_points$col)  ) +
  coord_sf(datum=NA) + 
  #
  #
  theme_bw() + 
  theme(legend.position = 'bottom',
        legend.background = element_rect(fill = "white"),
        legend.direction='vertical')+
  #
  #
  ggtitle("Natural resources in Quebec") +
  labs(caption= expression(italic('Mines: www.donneesquebec.ca. Wood industry: www.mffp.gouv.qc.ca. Hydroelectricity: CEHQ\nPopulation: Statistics Canada 2016 census. Author: @charles_gauvin') ))



ggsave(here('Figures', 'natual_resources_native.png'  ),
        p,
       height = 10,
       width = 7)

```